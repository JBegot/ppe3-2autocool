-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 17 avr. 2019 à 00:26
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `autocool-php`
--

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `codeCategorie` varchar(128) NOT NULL,
  `libelleCategorie` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`codeCategorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`codeCategorie`, `libelleCategorie`) VALUES
('L', 'Large'),
('M', 'Medium'),
('S', 'Small');

-- --------------------------------------------------------

--
-- Structure de la table `desservir`
--

DROP TABLE IF EXISTS `desservir`;
CREATE TABLE IF NOT EXISTS `desservir` (
  `codeArret` int(2) NOT NULL,
  `numStation` int(2) NOT NULL,
  PRIMARY KEY (`codeArret`,`numStation`),
  KEY `I_FK_Rel_1_Tramway` (`codeArret`),
  KEY `I_FK_Rel_1_Station` (`numStation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `desservir`
--

INSERT INTO `desservir` (`codeArret`, `numStation`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `fonction`
--

DROP TABLE IF EXISTS `fonction`;
CREATE TABLE IF NOT EXISTS `fonction` (
  `codeFonction` varchar(128) NOT NULL,
  `libelleFonction` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`codeFonction`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `fonction`
--

INSERT INTO `fonction` (`codeFonction`, `libelleFonction`) VALUES
('ABO', 'Abonné'),
('INT', 'Internaute'),
('RT', 'Responsable technique'),
('SEC', 'Secrétaire');

-- --------------------------------------------------------

--
-- Structure de la table `formule`
--

DROP TABLE IF EXISTS `formule`;
CREATE TABLE IF NOT EXISTS `formule` (
  `codeFormule` int(2) NOT NULL AUTO_INCREMENT,
  `libelleFormule` varchar(128) DEFAULT NULL,
  `fraisAdhesion` varchar(128) DEFAULT NULL,
  `tarifMensuel` varchar(128) DEFAULT NULL,
  `partSociale` varchar(128) DEFAULT NULL,
  `depotGarantie` varchar(128) DEFAULT NULL,
  `caution` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`codeFormule`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `formule`
--

INSERT INTO `formule` (`codeFormule`, `libelleFormule`, `fraisAdhesion`, `tarifMensuel`, `partSociale`, `depotGarantie`, `caution`) VALUES
(1, 'Coopérative', NULL, NULL, NULL, NULL, NULL),
(2, 'Classique\r\n', NULL, NULL, NULL, NULL, NULL),
(3, 'Liberté\r\n', NULL, NULL, NULL, NULL, NULL),
(4, 'Non adhérent', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `informationbancaire`
--

DROP TABLE IF EXISTS `informationbancaire`;
CREATE TABLE IF NOT EXISTS `informationbancaire` (
  `numPaiement` int(2) NOT NULL AUTO_INCREMENT,
  `numUtilisateur` int(2) NOT NULL,
  `titulaire` varchar(32) DEFAULT NULL,
  `compte_cle` varchar(32) DEFAULT NULL,
  `nomBanque` varchar(32) DEFAULT NULL,
  `banque_guichet` varchar(32) DEFAULT NULL,
  `IBAN` varchar(50) DEFAULT NULL,
  `BIC` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`numPaiement`),
  UNIQUE KEY `I_FK_InformationBancaire_Utilisateur` (`numUtilisateur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `modefacturation`
--

DROP TABLE IF EXISTS `modefacturation`;
CREATE TABLE IF NOT EXISTS `modefacturation` (
  `codeModeF` varchar(128) NOT NULL,
  `libelleModeF` char(32) DEFAULT NULL,
  PRIMARY KEY (`codeModeF`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `modefacturation`
--

INSERT INTO `modefacturation` (`codeModeF`, `libelleModeF`) VALUES
('COURRIER', 'Courrier'),
('MAIL', 'Email');

-- --------------------------------------------------------

--
-- Structure de la table `modepaiement`
--

DROP TABLE IF EXISTS `modepaiement`;
CREATE TABLE IF NOT EXISTS `modepaiement` (
  `codePaiement` int(2) NOT NULL AUTO_INCREMENT,
  `libellePaiement` char(32) DEFAULT NULL,
  PRIMARY KEY (`codePaiement`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `modepaiement`
--

INSERT INTO `modepaiement` (`codePaiement`, `libellePaiement`) VALUES
(1, 'Chèque'),
(2, 'Carte Bancaire'),
(3, 'PayPal');

-- --------------------------------------------------------

--
-- Structure de la table `parking`
--

DROP TABLE IF EXISTS `parking`;
CREATE TABLE IF NOT EXISTS `parking` (
  `numParking` int(2) NOT NULL AUTO_INCREMENT,
  `numStation` int(2) NOT NULL,
  `nomParking` varchar(128) DEFAULT NULL,
  `niveau` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`numParking`),
  KEY `I_FK_Parking_Station` (`numStation`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `parking`
--

INSERT INTO `parking` (`numParking`, `numStation`, `nomParking`, `niveau`) VALUES
(1, 2, 'Parking Cité Mondiale', '-2');

-- --------------------------------------------------------

--
-- Structure de la table `payertarifh`
--

DROP TABLE IF EXISTS `payertarifh`;
CREATE TABLE IF NOT EXISTS `payertarifh` (
  `codeTarifH` int(2) NOT NULL,
  `codeCategorie` varchar(128) NOT NULL,
  `codeFormule` int(2) NOT NULL,
  `montant` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`codeTarifH`,`codeCategorie`,`codeFormule`),
  KEY `I_FK_PayerTarifH_TarifHoraire` (`codeTarifH`),
  KEY `I_FK_PayerTarifH_Categorie` (`codeCategorie`),
  KEY `I_FK_PayerTarifH_Formule` (`codeFormule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `payertarifkm`
--

DROP TABLE IF EXISTS `payertarifkm`;
CREATE TABLE IF NOT EXISTS `payertarifkm` (
  `codeTarifKM` int(2) NOT NULL,
  `codeCategorie` varchar(128) NOT NULL,
  `codeFormule` int(2) NOT NULL,
  `montant` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`codeTarifKM`,`codeCategorie`,`codeFormule`),
  KEY `I_FK_PayerTarifKM_TarifKm` (`codeTarifKM`),
  KEY `I_FK_PayerTarifKM_Categorie` (`codeCategorie`),
  KEY `I_FK_PayerTarifKM_Formule` (`codeFormule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `reserver`
--

DROP TABLE IF EXISTS `reserver`;
CREATE TABLE IF NOT EXISTS `reserver` (
  `numVehicule` int(2) NOT NULL,
  `numUtilisateur` int(2) NOT NULL,
  `dateReservation` date NOT NULL,
  `heureReservation` time NOT NULL,
  `dateDebut` date DEFAULT NULL,
  `dateFin` date DEFAULT NULL,
  `heureDebut` time DEFAULT NULL,
  `heureFin` time DEFAULT NULL,
  `etatProprete` smallint(1) DEFAULT NULL,
  `degats` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`numVehicule`,`numUtilisateur`,`dateReservation`,`heureReservation`),
  KEY `I_FK_Reserver_Vehicule` (`numVehicule`),
  KEY `I_FK_Reserver_Utilisateur` (`numUtilisateur`),
  KEY `I_FK_Reserver_DateReservation` (`dateReservation`,`heureReservation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `reserver`
--

INSERT INTO `reserver` (`numVehicule`, `numUtilisateur`, `dateReservation`, `heureReservation`, `dateDebut`, `dateFin`, `heureDebut`, `heureFin`, `etatProprete`, `degats`) VALUES
(1, 2, '2019-04-16', '12:20:42', '2019-04-16', '2019-04-16', '00:00:00', '00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `station`
--

DROP TABLE IF EXISTS `station`;
CREATE TABLE IF NOT EXISTS `station` (
  `numStation` int(2) NOT NULL AUTO_INCREMENT,
  `villeStation` varchar(128) DEFAULT NULL,
  `lieu` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`numStation`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `station`
--

INSERT INTO `station` (`numStation`, `villeStation`, `lieu`) VALUES
(1, 'Bordeaux', 'Chartrons'),
(2, 'Bordeaux', 'Cité Mondiale'),
(3, 'Bordeaux', 'Barrière St Médard');

-- --------------------------------------------------------

--
-- Structure de la table `tarifhoraire`
--

DROP TABLE IF EXISTS `tarifhoraire`;
CREATE TABLE IF NOT EXISTS `tarifhoraire` (
  `codeTarifH` int(2) NOT NULL AUTO_INCREMENT,
  `libelleTarifH` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`codeTarifH`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `tarifkm`
--

DROP TABLE IF EXISTS `tarifkm`;
CREATE TABLE IF NOT EXISTS `tarifkm` (
  `codeTarifKM` int(2) NOT NULL AUTO_INCREMENT,
  `libelleTarifM` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`codeTarifKM`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `tramway`
--

DROP TABLE IF EXISTS `tramway`;
CREATE TABLE IF NOT EXISTS `tramway` (
  `codeArret` int(2) NOT NULL AUTO_INCREMENT,
  `libelleArret` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`codeArret`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tramway`
--

INSERT INTO `tramway` (`codeArret`, `libelleArret`) VALUES
(1, 'Ligne C Paul Doumer'),
(2, 'Ligne B CAPC'),
(3, 'Ligne B Quinconces'),
(4, 'Ligne C Quinconces'),
(5, 'Ligne C Place de la Bourse');

-- --------------------------------------------------------

--
-- Structure de la table `typevehicule`
--

DROP TABLE IF EXISTS `typevehicule`;
CREATE TABLE IF NOT EXISTS `typevehicule` (
  `codeType` varchar(128) NOT NULL,
  `codeCategorie` varchar(128) NOT NULL,
  `libelleType` char(32) DEFAULT NULL,
  `nbPlaces` char(32) DEFAULT NULL,
  PRIMARY KEY (`codeType`),
  KEY `I_FK_TypeVehicule_Categorie` (`codeCategorie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `typevehicule`
--

INSERT INTO `typevehicule` (`codeType`, `codeCategorie`, `libelleType`, `nbPlaces`) VALUES
('BREAK', 'L', 'Ludospace', '7'),
('CITY', 'L', 'Citadine', '4'),
('POLY', 'M', 'Polyvalente', '5'),
('UTIL', 'L', 'Ludospace', '5');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `numUtilisateur` int(2) NOT NULL AUTO_INCREMENT,
  `codePaiement` int(2) NOT NULL,
  `codeFonction` varchar(128) NOT NULL,
  `codeModeF` varchar(128) NOT NULL,
  `codeFormule` int(2) NOT NULL,
  `nom` varchar(128) NOT NULL,
  `prenom` varchar(128) NOT NULL,
  `dateNaissance` date DEFAULT NULL,
  `adresse` varchar(255) NOT NULL,
  `sexe` varchar(128) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `telephone` varchar(128) DEFAULT NULL,
  `numPermis` int(2) DEFAULT NULL,
  `lieuObtention` varchar(128) DEFAULT NULL,
  `dateObtention` date DEFAULT NULL,
  `mdp` varchar(255) NOT NULL,
  PRIMARY KEY (`numUtilisateur`),
  KEY `I_FK_Utilisateur_ModePaiement` (`codePaiement`),
  KEY `I_FK_Utilisateur_Fonction` (`codeFonction`),
  KEY `I_FK_Utilisateur_ModeFacturation` (`codeModeF`),
  KEY `I_FK_Utilisateur_Formule` (`codeFormule`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`numUtilisateur`, `codePaiement`, `codeFonction`, `codeModeF`, `codeFormule`, `nom`, `prenom`, `dateNaissance`, `adresse`, `sexe`, `email`, `telephone`, `numPermis`, `lieuObtention`, `dateObtention`, `mdp`) VALUES
(2, 1, 'INT', 'COURRIER', 4, 'BETOURNE', 'Mattis', '2019-04-18', '5 rue des portes de CaudÃ©ran', 'homme', 'mattis.betourne@gmail.com', '0638695549', 86513865, 'Bordeaux', '2019-04-10', '$2y$10$LcrV6QJEeB7ayXSU6Nrscex2X.2AKt8nsTHaMwab90jV3FyPfRIEi'),
(5, 1, 'INT', 'COURRIER', 4, 'BETOURNE', 'Mattis', '2019-04-18', '5 rue des portes de CaudÃ©ran', 'homme', 'mattis.ebetourne@gmail.com', '0638695549', 86513865, 'Bordeaux', '2019-04-10', '$2y$10$1MllTxTaGUsnynFh2YkvzuDN.NTiQXfsChVREzqD0vjlqg8SnmikK');

-- --------------------------------------------------------

--
-- Structure de la table `vehicule`
--

DROP TABLE IF EXISTS `vehicule`;
CREATE TABLE IF NOT EXISTS `vehicule` (
  `numVehicule` int(2) NOT NULL AUTO_INCREMENT,
  `codeType` varchar(128) NOT NULL,
  `numStation` int(2) NOT NULL,
  `kilometrage` int(2) DEFAULT NULL,
  `niveauEssence` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`numVehicule`),
  KEY `I_FK_Vehicule_TypeVehicule` (`codeType`),
  KEY `I_FK_Vehicule_Station` (`numStation`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `vehicule`
--

INSERT INTO `vehicule` (`numVehicule`, `codeType`, `numStation`, `kilometrage`, `niveauEssence`) VALUES
(1, 'BREAK', 1, 276, '1/2'),
(2, 'CITY', 1, 276, '1/2');

-- --------------------------------------------------------

--
-- Structure de la table `voirie`
--

DROP TABLE IF EXISTS `voirie`;
CREATE TABLE IF NOT EXISTS `voirie` (
  `numVoirie` int(2) NOT NULL AUTO_INCREMENT,
  `numStation` int(2) NOT NULL,
  `adresse` varchar(128) DEFAULT NULL,
  `nbPlaces` int(2) DEFAULT NULL,
  PRIMARY KEY (`numVoirie`),
  KEY `I_FK_Voirie_Station` (`numStation`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `voirie`
--

INSERT INTO `voirie` (`numVoirie`, `numStation`, `adresse`, `nbPlaces`) VALUES
(1, 1, '17 rue Sicard', 15),
(2, 3, '274 Bd Wilson', 15);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `desservir`
--
ALTER TABLE `desservir`
  ADD CONSTRAINT `FK_Rel_1_Station` FOREIGN KEY (`numStation`) REFERENCES `station` (`numStation`),
  ADD CONSTRAINT `FK_Rel_1_Tramway` FOREIGN KEY (`codeArret`) REFERENCES `tramway` (`codeArret`);

--
-- Contraintes pour la table `informationbancaire`
--
ALTER TABLE `informationbancaire`
  ADD CONSTRAINT `FK_InformationBancaire_Utilisateur` FOREIGN KEY (`numUtilisateur`) REFERENCES `utilisateur` (`numUtilisateur`);

--
-- Contraintes pour la table `parking`
--
ALTER TABLE `parking`
  ADD CONSTRAINT `FK_Parking_Station` FOREIGN KEY (`numStation`) REFERENCES `station` (`numStation`);

--
-- Contraintes pour la table `payertarifh`
--
ALTER TABLE `payertarifh`
  ADD CONSTRAINT `FK_PayerTarifH_Categorie` FOREIGN KEY (`codeCategorie`) REFERENCES `categorie` (`codeCategorie`),
  ADD CONSTRAINT `FK_PayerTarifH_Formule` FOREIGN KEY (`codeFormule`) REFERENCES `formule` (`codeFormule`),
  ADD CONSTRAINT `FK_PayerTarifH_TarifHoraire` FOREIGN KEY (`codeTarifH`) REFERENCES `tarifhoraire` (`codeTarifH`);

--
-- Contraintes pour la table `payertarifkm`
--
ALTER TABLE `payertarifkm`
  ADD CONSTRAINT `FK_PayerTarifKM_Categorie` FOREIGN KEY (`codeCategorie`) REFERENCES `categorie` (`codeCategorie`),
  ADD CONSTRAINT `FK_PayerTarifKM_Formule` FOREIGN KEY (`codeFormule`) REFERENCES `formule` (`codeFormule`),
  ADD CONSTRAINT `FK_PayerTarifKM_TarifKm` FOREIGN KEY (`codeTarifKM`) REFERENCES `tarifkm` (`codeTarifKM`);

--
-- Contraintes pour la table `reserver`
--
ALTER TABLE `reserver`
  ADD CONSTRAINT `FK_Reserver_Utilisateur` FOREIGN KEY (`numUtilisateur`) REFERENCES `utilisateur` (`numUtilisateur`),
  ADD CONSTRAINT `FK_Reserver_Vehicule` FOREIGN KEY (`numVehicule`) REFERENCES `vehicule` (`numVehicule`);

--
-- Contraintes pour la table `typevehicule`
--
ALTER TABLE `typevehicule`
  ADD CONSTRAINT `FK_TypeVehicule_Categorie` FOREIGN KEY (`codeCategorie`) REFERENCES `categorie` (`codeCategorie`);

--
-- Contraintes pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD CONSTRAINT `FK_Utilisateur_Fonction` FOREIGN KEY (`codeFonction`) REFERENCES `fonction` (`codeFonction`),
  ADD CONSTRAINT `FK_Utilisateur_Formule` FOREIGN KEY (`codeFormule`) REFERENCES `formule` (`codeFormule`),
  ADD CONSTRAINT `FK_Utilisateur_ModeFacturation` FOREIGN KEY (`codeModeF`) REFERENCES `modefacturation` (`codeModeF`),
  ADD CONSTRAINT `FK_Utilisateur_ModePaiement` FOREIGN KEY (`codePaiement`) REFERENCES `modepaiement` (`codePaiement`);

--
-- Contraintes pour la table `vehicule`
--
ALTER TABLE `vehicule`
  ADD CONSTRAINT `FK_Vehicule_Station` FOREIGN KEY (`numStation`) REFERENCES `station` (`numStation`),
  ADD CONSTRAINT `FK_Vehicule_TypeVehicule` FOREIGN KEY (`codeType`) REFERENCES `typevehicule` (`codeType`);

--
-- Contraintes pour la table `voirie`
--
ALTER TABLE `voirie`
  ADD CONSTRAINT `FK_Voirie_Station` FOREIGN KEY (`numStation`) REFERENCES `station` (`numStation`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
