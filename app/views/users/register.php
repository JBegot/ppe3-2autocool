<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt-5">
            <h2>S'inscrire</h2>
            <form action="<?php echo URLROOT; ?>/users/register" method="post">
                <div class="form-row">
                    <div class="col">
                        <label for="email">Email:</label>
                        <input type="text" name="email" class="form-control  <?php echo (!empty($data['email_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['email']; ?>">
                        <span class="invalid-feedback"><?php echo $data['email_err']; ?></span>
                    </div>
                    <div class="col">
                        <label for="password">Mot de passe:</label>
                        <input type="password" name="password" class="form-control <?php echo (!empty($data['password_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['password']; ?>">
                        <span class="invalid-feedback"><?php echo $data['password_err']; ?></span>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <label for="nom">Nom:</label>
                        <input type="text" name="nom" class="form-control <?php echo (!empty($data['nom_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['nom']; ?>">
                        <span class="invalid-feedback"><?php echo $data['nom_err']; ?></span>
                    </div>
                    <div class="col">
                        <label for="prenom">Prénom:</label>
                        <input type="text" name="prenom" class="form-control <?php echo (!empty($data['prenom_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['prenom']; ?>">
                        <span class="invalid-feedback"><?php echo $data['prenom_err']; ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="dateNaissance">Date de naissance:</label>
                    <input type="date" name="dateNaissance" class="form-control <?php echo (!empty($data['dateNaissance_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['dateNaissance']; ?>">
                    <span class="invalid-feedback"><?php echo $data['dateNaissance_err']; ?></span>
                </div>
                <div class="form-group">
                    <label for="adresse">Adresse:</label>
                    <input type="text" name="adresse" class="form-control <?php echo (!empty($data['adresse_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['adresse']; ?>">
                    <span class="invalid-feedback"><?php echo $data['adresse_err']; ?></span>
                </div>
                <div class="form-group">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" checked type="radio" name="sexe" id="homme" value="homme">
                        <label class="form-check-label" for="homme">Homme</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sexe" id="femme" value="femme">
                        <label class="form-check-label" for="femme">Femme</label>
                        <span class="invalid-feedback"><?php echo $data['sexe_err']; ?></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="telephone">Téléphone:</label>
                    <input type="text" name="telephone" class="form-control <?php echo (!empty($data['telephone_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['telephone']; ?>">
                    <span class="invalid-feedback"><?php echo $data['telephone_err']; ?></span>
                </div>

                <div class="form-group">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" checked type="radio" name="modePaiement" id="cheque" value="1">
                        <label class="form-check-label" for="homme">Chèque</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="modePaiement" id="carteBancaire" value="2">
                        <label class="form-check-label" for="femme">Carte Bancaire</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="modePaiement" id="paypal" value="3">
                        <label class="form-check-label" for="femme">PayPal</label>
                        <span class="invalid-feedback"><?php echo $data['modePaiement_err'];?></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" checked type="radio" name="modeFacturation" id="Courrier" value="COURRIER">
                        <label class="form-check-label" for="homme">Courrier</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="modeFacturation" id="Mail" value="MAIL">
                        <label class="form-check-label" for="femme">Mail</label>
                        <span class="invalid-feedback"><?php echo $data['modeFacturation_err']; ?></span>
                    </div>
                </div>

                <div class="form-row mb-3">
                    <div class="col">
                        <label for="lieuObtentionPermis">Lieu d'obtention:</label>
                        <input type="text" name="lieuObtentionPermis" class="form-control <?php echo (!empty($data['lieuObtentionPermis_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['lieuObtentionPermis']; ?>">
                        <span class="invalid-feedback"><?php echo $data['lieuObtentionPermis_err']; ?></span>
                    </div>
                    <div class="col">
                        <label for="dateObtentionPermis">Date d'obtention:</label>
                        <input type="date" name="dateObtentionPermis" class="form-control <?php echo (!empty($data['dateObtentionPermis_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['dateObtentionPermis']; ?>">
                        <span class="invalid-feedback"><?php echo $data['dateObtentionPermis_err']; ?></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="numPermis">N° de permis:</label>
                    <input type="text" name="numPermis" class="form-control <?php echo (!empty($data['numPermis_err'])) ? 'is-invalid' : '';?>" value="<?php echo $data['numPermis']; ?>">
                    <span class="invalid-feedback"><?php echo $data['numPermis_err']; ?></span>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="S'inscrire" class="btn btn-primary btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php require APPROOT . '/views/inc/footer.php'; ?>