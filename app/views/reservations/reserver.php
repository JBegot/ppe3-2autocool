<?php require APPROOT . '/views/inc/header.php'; ?>
    <div class="row">
        <div class="col-md-6 mx-auto">
            <div class="card card-body bg-light mt-5 shadow rounded">
                <h2>Réserver</h2>
                <form action="<?php echo URLROOT; ?>/reservations/reserver" method="post">
                    <?php if(is_array($data['stations'])): ?>
                    <div class="form-group">
                        <select class="custom-select <?php echo (!empty($data['stations_err'])) ? 'is-invalid' : '';?>" name="stations">
                            <option value="">Choisissez une station</option>
                            <?php foreach ($data['stations'] as $station) : ?>
                                <option value="<?php  echo $station->numStation; ?>"><?php echo $station->villeStation; ?> - <?php echo $station->lieu; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <span class="invalid-feedback"><?php echo $data['stations_err']; ?></span>
                    </div>
                    <?php  else: ?>
                        <div class="form-group">
                            <select class="custom-select <?php echo (!empty($data['stations_err'])) ? 'is-invalid' : '';?>" name="stations">
                                <option value="<?php  echo $data['stations']->numStation; ?>"><?php echo $data['stations']->villeStation; ?> - <?php echo $data['stations']->lieu; ?></option>
                            </select>
                            <span class="invalid-feedback"><?php echo $data['stations_err']; ?></span>
                        </div>
                    <?php endif; ?>


                    <div class="form-row">
                        <div class="col">
                            <label>
                                Date début
                                <input type="date" class="form-control <?php echo (!empty($data['dateDebut_err'])) ? 'is-invalid' : '';?>" name="dateDebut" value="<?php echo $data['dateDebut']; ?>">
                                <span class="invalid-feedback"><?php echo $data['dateDebut_err']; ?></span>
                            </label>

                        </div>
                        <div class="col">
                            <label>
                                Heure début
                                <input type="time" class="form-control <?php echo (!empty($data['heureDebut_err'])) ? 'is-invalid' : '';?>" name="heureDebut" value="<?php echo $data['heureDebut']; ?>">
                                <span class="invalid-feedback"><?php echo $data['heureDebut_err']; ?></span>
                            </label>

                        </div>
                    </div>
                    <div class="form-row mb-3">
                        <div class="col">
                            <label>
                                Date fin
                                <input type="date" class="form-control <?php echo (!empty($data['dateFin_err'])) ? 'is-invalid' : '';?>" name="dateFin" value="<?php echo $data['dateFin']; ?>">
                                <span class="invalid-feedback"><?php echo $data['dateFin_err']; ?></span>
                            </label>

                        </div>
                        <div class="col">
                            <label>
                                Heure fin
                                <input type="time" class="form-control <?php echo (!empty($data['heureFin_err'])) ? 'is-invalid' : '';?>" name="heureFin" value="<?php echo $data['heureFin']; ?>">
                                <span class="invalid-feedback"><?php echo $data['heureFin_err']; ?></span>
                            </label>

                        </div>
                    </div>

                    <?php if(!empty($data['vehicules'])) : ?>
                        <div class="form-group">
                                <select class="custom-select <?php echo (!empty($data['vehicules_err'])) ? 'is-invalid' : '';?>" name="vehicules">
                                    <option value="">Choisissez un vehicule</option>
                                    <?php foreach ($data['vehicules'] as $vehicule) : ?>
                                        <option value="<?php  echo $vehicule->numVehicule; ?>"><?php echo $vehicule->codeType; ?> - <?php echo $vehicule->kilometrage; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            <span class="invalid-feedback"><?php echo $data['vehicules_err']; ?></span>
                        </div>
                        <button class="btn btn-primary" name="reserver">Réserver</button>
                    <?php else: ?>
                        <span class="invalid-feedback"><?php echo $data['vehicules_err']; ?></span>
                        <button class="btn btn-primary" name="checkVehicules">Voir véhicule disponible pour ce créneau</button>
                    <?php endif; ?>



                </form>
            </div>
        </div>
    </div>
<?php require APPROOT . '/views/inc/footer.php'; ?>