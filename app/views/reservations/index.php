<?php require APPROOT . '/views/inc/header.php'; ?>
<div class="row">
    <div class="col-md-6 text-left text-white"><h2>Réservations</h2></div>
</div>

<div class="row">
    <table class="table table-striped table-dark table-bordered">
        <thead>
        <tr>
            <th scope="col">Le</th>
            <th scope="col">à</th>
            <th scope="col">Date de début</th>
            <th scope="col">Heure de début</th>
            <th scope="col">Date de fin</th>
            <th scope="col">Heure de fin</th>
            <th scope="col">Vehicule</th>
            <th scope="col">Kilométrage</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data['reservations'] as $reservation) : ?>
            <tr>
                <td><?php echo $reservation->dateReservation; ?></td>
                <td><?php echo $reservation->heureReservation; ?></td>
                <td><?php echo $reservation->dateDebut; ?></td>
                <td><?php echo $reservation->heureDebut; ?></td>
                <td><?php echo $reservation->dateFin; ?></td>
                <td><?php echo $reservation->heureFin; ?></td>
                <td><?php echo $reservation->codeType; ?></td>
                <td><?php echo $reservation->kilometrage; ?> km</td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<div class="row">
    <div class="col-md-6 mb-4"><a href="<?php echo URLROOT ?>/reservations/reserver" class="btn btn-primary">Réserver votre véhicule</a></div>
</div>

<?php require APPROOT . '/views/inc/footer.php'; ?>