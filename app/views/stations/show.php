<?php require APPROOT . '/views/inc/header.php'; ?>
<h1 class="text-center text-white"><?php echo $data['station']->villeStation; ?> - <?php echo $data['station']->lieu; ?></h1>

<div class="row text-center text-white mb-3">
    <div class="col">
        <h3>Voirie</h3>
        <?php foreach ($data['voiries'] as $voirie) :?>
            <div class="card card-body bg-dark mb-3 shadow rounded">
                <h4 class="card-title"><?php  echo $voirie->adresse; ?></h4>
                <h6 class="card-subtitle mb-2 text-muted"><?php  echo $voirie->nbPlaces; ?> places</h6>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="col">
        <h3>Parking</h3>
        <?php foreach ($data['parkings'] as $parking) :?>
            <div class="card card-body bg-dark mb-3 shadow rounded">
                        <h4 class="card-title"><?php  echo $parking->nomParking; ?></h4>
                        <h6 class="card-subtitle mb-2 text-muted">Niveau <?php  echo $parking->niveau; ?></h6>
            </div>
        <?php endforeach; ?>
    </div>
</div>
<?php if (!empty($data['vehicules'])) : ?>
<h3 class="text-white">Véhicules disponibles</h3>
<?php foreach ($data['vehicules'] as $vehicule) :?>
    <div class="card card-body bg-dark mb-3 shadow rounded text-white">
        <h4 class="card-title"><?php  echo ucfirst(strtolower($vehicule->codeType)); ?> <?php  echo $vehicule->nbPlaces; ?> places</h4>
        <h6 class="card-subtitle mb-2 text-muted"><?php  echo $vehicule->kilometrage; ?> km</h6>
    </div>
<?php endforeach; ?>
<?php endif; ?>

<?php if (!empty($data['tramways'])) : ?>
    <h3 class="text-white">Tramways</h3>
    <?php foreach ($data['tramways'] as $tramway) :?>
        <div class="card card-body bg-dark mb-3 shadow rounded text-white">
            <h4 class="card-title"><?php  echo $tramway->libelleArret; ?></h4>
        </div>
    <?php endforeach; ?>
<?php endif; ?>

<?php require APPROOT . '/views/inc/footer.php'; ?>