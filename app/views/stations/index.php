<?php require APPROOT . '/views/inc/header.php'; ?>
    <div class="row col-md-6 offset-md-3 mt-4">
        <?php foreach ($data['stations'] as $station) : ?>
            <div class="card card-body mb-3">
                <div class="row">
                    <div class="col">
                        <h4 class="card-title"><?php echo $station->villeStation; ?></h4>
                        <h6 class="card-subtitle mb-2 text-muted"><?php echo $station->lieu; ?></h6>
                    </div>
                    <div class="col text-right mt-3">
                        <a class="btn btn-primary"
                           href="<?php echo URLROOT ?>/stations/show/<?php echo $station->numStation; ?>">Consulter la
                            station <i class="fas fa-eye"></i></a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php require APPROOT . '/views/inc/footer.php'; ?>