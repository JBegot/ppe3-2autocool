<?php

class Stations extends Controller
{
    private $stationModel;
    private $vehiculeModel;
    private $tramwayModel;

    public function __construct()
    {
        if (!isLoggedIn()){
            redirect('users/login');
        }

        $this->stationModel = $this->model('Station');
        $this->vehiculeModel = $this->model('Vehicule');
        $this->tramwayModel = $this->model('Tramway');
    }

    public function index(){
        $stations = $this->stationModel->getStations();

        $data = [
            'stations' => $stations
        ];

        $this->view('stations/index', $data);
    }

    public function show($id){
        $station = $this->stationModel->getStationById($id);
        $voiries = $this->stationModel->getVoiriesById($id);
        $parkings = $this->stationModel->getParkingsById($id);
        $vehicules = $this->vehiculeModel->getVehiculesByStationId($id);
        $tramways = $this->tramwayModel->getTramwaysByStationId($id);

        $data = [
            'station' => $station,
            'voiries' => $voiries,
            'parkings' => $parkings,
            'vehicules' => $vehicules,
            'tramways' => $tramways
        ];
        $this->view('stations/show', $data);
    }
}