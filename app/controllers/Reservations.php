<?php

class Reservations extends Controller
{
    private $stationModel;
    private $vehiculeModel;
    private $reserverModel;

    public function __construct()
    {
        if (!isLoggedIn()){
            redirect('users/login');
        }

        $this->stationModel = $this->model('Station');
        $this->vehiculeModel = $this->model('Vehicule');
        $this->reserverModel = $this->model("Reserver");
    }


    public function index(){

        $reservations = $this->reserverModel->getReservationsByUser($_SESSION['user_id']);

        $data = [
            'reservations' => $reservations
        ];

        $this->view("reservations/index", $data);
    }

    public function reserver(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            if(isset($_POST['checkVehicules'])){

                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                $data = [
                    'stations' => trim($_POST['stations']),
                    'vehicules' => '',
                    'dateDebut' => trim($_POST['dateDebut']),
                    'heureDebut' => trim($_POST['heureDebut']),
                    'dateFin' => trim($_POST['dateFin']),
                    'heureFin' => trim($_POST['heureFin']),

                    'stations_err' => '',
                    'dateDebut_err' => '',
                    'heureDebut_err' => '',
                    'dateFin_err' => '',
                    'heureFin_err' => '',
                    'vehicules_err' => ''
                ];

                if(empty($data['stations'])){
                    $data['stations_err'] = "Vous devez choisir une station";
                } else {
                    $id = $data['stations'];
                    $data['stations'] = $this->stationModel->getStationById($id);

                    if(!empty($this->vehiculeModel->getVehiculesByStationId($id))){
                        $data['vehicules'] = $this->vehiculeModel->getVehiculesByStationId($id);
                    } else {
                        $data['vehicules_err'] = "Aucun vehicule disponible";
                    }
                }

                if(empty($data['dateDebut'])){
                    $data['dateDebut_err'] = "Vous devez choisir une date";
                }

                if(empty($data['heureDebut'])){
                    $data['heureDebut_err'] = "Vous devez choisir une heure";
                }

                if(empty($data['dateFin'])){
                    $data['dateFin_err'] = "Vous devez choisir une date";
                }

                if(empty($data['heureFin'])){
                    $data['heureFin_err'] = "Vous devez choisir une heure";
                }

                // Verify date time
                $dateDebut = new DateTime($data['dateDebut']);
                $dateFin = new DateTime($data['dateFin']);

                if ($dateDebut > $dateFin) {
                    $data['dateDebut_err'] = "Erreur, la date de début est supérieur à la date de fin";
                    $data['dateFin_err'] = "Erreur, la date de début est supérieur à la date de fin";
                }


                if (empty($data['vehicules_err']) && empty($data['stations_err']) && empty($data['dateDebut_err']) && empty($data['heureDebut_err']) && empty($data['dateFin_err']) && empty($data['heureFin_err'])){

                    $this->view('reservations/reserver', $data);
                } else {
                    $data['stations'] = $this->stationModel->getStations();
                    $data['vehicules'] = '';
                    $this->view('reservations/reserver', $data);
                }
            }
            elseif (isset($_POST['reserver'])){

                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                $data = [
                    'numUtilisateur' => $_SESSION['user_id'],
                    'stations' => trim($_POST['stations']),
                    'vehicules' => trim($_POST['vehicules']),
                    'dateDebut' => trim($_POST['dateDebut']),
                    'heureDebut' => trim($_POST['heureDebut']),
                    'dateFin' => trim($_POST['dateFin']),
                    'heureFin' => trim($_POST['heureFin']),

                    'stations_err' => '',
                    'dateDebut_err' => '',
                    'heureDebut_err' => '',
                    'dateFin_err' => '',
                    'heureFin_err' => '',
                    'vehicules_err' => ''
                ];

                if(empty($data['vehicules'])){
                    $data['vehicules_err'] = "Vous devez choisir un vehicule";
                }

                if(empty($data['dateDebut'])){
                    $data['dateDebut_err'] = "Vous devez choisir une date";
                }

                if(empty($data['heureDebut'])){
                    $data['heureDebut_err'] = "Vous devez choisir une heure";
                }

                if(empty($data['dateFin'])){
                    $data['dateFin_err'] = "Vous devez choisir une date";
                }

                if(empty($data['heureFin'])){
                    $data['heureFin_err'] = "Vous devez choisir une heure";
                }

                // Verify date time
                $dateDebut = new DateTime($data['dateDebut']);
                $dateFin = new DateTime($data['dateFin']);

                if ($dateDebut > $dateFin) {
                    $data['dateDebut_err'] = "Erreur, la date de début est supérieur à la date de fin";
                    $data['dateFin_err'] = "Erreur, la date de début est supérieur à la date de fin";
                }

                if (empty($data['vehicules_err']) && empty($data['stations_err']) && empty($data['dateDebut_err']) && empty($data['heureDebut_err']) && empty($data['dateFin_err']) && empty($data['heureFin_err'])){

                    if($this->reserverModel->insert($data)){
                        flash("reservation_added", "Reservation envoyée");
                        redirect("reservations");
                    } else {
                        die("Something went wrong");
                    }

                    $this->view('reservations/reserver', $data);
                } else {
                    $this->view('reservations/reserver', $data);
                }



            }


        } else {
            $stations = $this->stationModel->getStations();

            $data = [
              'stations' => $stations,
              'station' => '',
              'vehicules' => '',
              'dateDebut' => '',
              'heureDebut' => '',
              'dateFin' => '',
              'heureFin' => '',

              'stations_err' => '',
              'dateDebut_err' => '',
              'heureDebut_err' => '',
              'dateFin_err' => '',
              'heureFin_err' => '',
            ];

            $this->view('reservations/reserver', $data);
        }
    }
}