<?php

class Users extends Controller
{
    private $userModel;

    public function __construct()
    {
        $this->userModel = $this->model('User');
    }

    public function register(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'nom' => trim($_POST['nom']),
                'prenom' => trim($_POST['prenom']),
                'dateNaissance' => trim($_POST['dateNaissance']),
                'adresse' => trim($_POST['adresse']),
                'sexe' => trim($_POST['sexe']),
                'email' => trim($_POST['email']),
                'telephone' => trim($_POST['telephone']),
                'numPermis' => trim($_POST['numPermis']),
                'lieuObtentionPermis' => trim($_POST['lieuObtentionPermis']),
                'dateObtentionPermis' => trim($_POST['dateObtentionPermis']),
                'modeFacturation' => trim($_POST['modeFacturation']),
                'modePaiement' => trim($_POST['modePaiement']),
                'password' => trim($_POST['password']),

                'nom_err' => '',
                'prenom_err' => '',
                'dateNaissance_err' => '',
                'adresse_err' => '',
                'sexe_err' => '',
                'email_err' => '',
                'telephone_err' => '',
                'numPermis_err' => '',
                'lieuObtentionPermis_err' => '',
                'dateObtentionPermis_err' => '',
                'modeFacturation_err' => '',
                'modePaiement_err' => '',
                'password_err' => '',
            ];

            if (empty($data['nom'])) {
                $data['nom_err'] = "Veuillez entrer un nom";
            }

            if (empty($data['prenom'])) {
                $data['prenom_err'] = "Veuillez entrer un prenom";
            }

            if (empty($data['dateNaissance'])) {
                $data['dateNaissance_err'] = "Veuillez entrer une date de naissance";
            }

            if (empty($data['adresse'])) {
                $data['adresse_err'] = "Veuillez entrer une adresse";
            }

            if (empty($data['sexe'])) {
                $data['sexe_err'] = "Veuillez saisir un genre";
            }

            if (empty($data['email'])) {
                $data['email_err'] = "Veuillez saisir une adresse e-mail";
            } else {
                if($this->userModel->findUserByEmail($data['email'])){
                    $data['email_err'] = 'Cette adresse email est déjà utilisée';
                }
            }

            if (empty($data['telephone'])) {
                $data['telephone_err'] = "Veuillez saisir un numéro de téléphone";
            }

            if (empty($data['numPermis'])) {
                $data['numPermis_err'] = "Veuillez saisir un numéro de permis";
            }

            if (empty($data['lieuObtentionPermis'])) {
                $data['lieuObtentionPermis_err'] = "Veuillez saisir un lieu d'obtention de permis";
            }

            if (empty($data['dateObtentionPermis'])) {
                $data['dateObtentionPermis_err'] = "Veuillez saisir une date d'obtention du permis";
            }

            if (empty($data['modeFacturation'])) {
                $data['modeFacturation_err'] = "Veuillez saisir un mode de facturation";
            }

            if (empty($data['modePaiement'])) {
                $data['modePaiement_err'] = "Veuillez saisir une mode de paiement";
            }

            if (empty($data['password'])) {
                $data['password_err'] = "Veuillez saisir un mot de passe";
            }

            if(empty($data['nom_err']) && empty($data['prenom_err']) && empty($data['dateNaissance_err']) && empty($data['adresse_err']) && empty($data['sexe_err']) && empty($data['email_err']) && empty($data['telephone_err']) && empty($data['numPermis_err'])
            && empty($data['lieuObtentionPermis_err']) && empty($data['dateObtentionPermis_err']) && empty($data['modeFacturation_err']) && empty($data['modePaiement_err']) && empty($data['password_err'])) {

                // Hash password
                $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);

                // Register User
                if($this->userModel->register($data)){
                    flash('register_success', 'Inscription réussie, vous pouvez vous connecter');
                    redirect('users/login');
                } else {
                    die('Something went wrong');
                }
            } else {
                $this->view('users/register', $data);
            }


        } else {
            $data = [
              'nom' => '',
              'prenom' => '',
              'dateNaissance' => '',
              'adresse' => '',
              'sexe' => '',
              'email' => '',
              'telephone' => '',
              'numPermis' => '',
              'lieuObtentionPermis' => '',
              'dateObtentionPermis' => '',
              'modePaiement' => '',
              'modeFacturation' => '',
              'password' => '',

                'nom_err' => '',
                'prenom_err' => '',
                'dateNaissance_err' => '',
                'adresse_err' => '',
                'sexe_err' => '',
                'email_err' => '',
                'telephone_err' => '',
                'numPermis_err' => '',
                'modePaiement_err' => '',
                'modeFacturation_err' => '',
                'password_err' => '',
            ];

            $this->view('users/register', $data);
        }
    }

    public function login(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            $data = [
                'email' => trim($_POST['email']),
                'password' => trim($_POST['password']),
                'email_err' => '',
                'password_err' => '',
            ];

            if (empty($data['email'])) {
                $data['email_err'] = "Veuillez saisir une adresse e-mail";
            }

            if (empty($data['password'])) {
                $data['password_err'] = "Veuillez saisir un mot de passe";
            }

            // Check for email
            if($this->userModel->findUserByEmail($data['email'])){

            } else {
                $data['email_err'] = "Email incorrect";
            }

            if(empty($data['email_err']) && empty($data['password_err'])){

                // Check and set logged in user
                $loggedInUser =$this->userModel->login($data['email'], $data['password']);

                if($loggedInUser){
                    // Create session
                    $this->createUserSession($loggedInUser);
                } else {
                    $data['password_err'] = "Mot de passe incorrect";

                    $this->view('users/login', $data);
                }
            } else {
                $this->view('users/login', $data);
            }


        } else {
            $data = [
                'email' => '',
                'password' => '',
                'email_err' => '',
                'password_err' => '',
            ];

            $this->view('users/login', $data);
        }
    }

    public function logout(){
        unset($_SESSION['user_id']);
        unset($_SESSION['user_email']);
        unset($_SESSION['user_name']);
        session_destroy();
        redirect('users/login');
    }

    private function createUserSession($user)
    {
        $_SESSION['user_id'] = $user->numUtilisateur;
        $_SESSION['user_email'] = $user->email;
        $_SESSION['user_name'] = $user->nom;
        redirect('pages/index');
    }
}