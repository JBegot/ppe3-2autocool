<?php

// DB PARAMS
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'autocool-php');

define('APPROOT', dirname(dirname(__FILE__)));
define('URLROOT', 'http://localhost/ppe3-2autocool');
define('SITENAME', 'PPE 3.2 Autocool');