<?php

class Tramway
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getTramwaysByStationId($id){
        $this->db->query("SELECT * FROM desservir INNER JOIN tramway t on desservir.codeArret = t.codeArret WHERE numStation = :id");
        $this->db->bind(':id', $id);

        $results = $this->db->resultSet();

        return $results;
    }
}