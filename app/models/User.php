<?php

class User
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function findUserByEmail($email){
        $this->db->query('SELECT * FROM utilisateur WHERE email = :email');
        $this->db->bind(':email', $email);
        $row = $this->db->single();

        if($this->db->rowCount() > 0){
            return true;
        } else {
            return false;
        }
    }

    public function register($data){
        $this->db->query('INSERT INTO utilisateur (numUtilisateur,mdp,email,codePaiement,codeFonction,codeModeF,codeFormule,nom,prenom,
                              dateNaissance,adresse,sexe,telephone,numPermis,lieuObtention,dateObtention) 
                              VALUES (:numUtilisateur,:mdp,:email,:codePaiement,:codeFonction,:codeModeF,:codeFormule,:nom,:prenom,
                              :dateNaissance,:adresse,:sexe,:telephone,:numPermis,:lieuObtention,:dateObtention)');
        $this->db->bind(':numUtilisateur', NULL);
        $this->db->bind(':mdp', $data['password']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':codePaiement', $data['modePaiement']);
        $this->db->bind(':codeFonction', 'INT');
        $this->db->bind(':codeModeF', $data['modeFacturation']);
        $this->db->bind(':codeFormule', 4);
        $this->db->bind(':nom', $data['nom']);
        $this->db->bind(':prenom', $data['prenom']);
        $this->db->bind(':dateNaissance', $data['dateNaissance']);
        $this->db->bind(':adresse', $data['adresse']);
        $this->db->bind(':sexe', $data['sexe']);
        $this->db->bind(':telephone', $data['telephone']);
        $this->db->bind(':numPermis', $data['numPermis']);
        $this->db->bind(':lieuObtention', $data['lieuObtentionPermis']);
        $this->db->bind(':dateObtention', $data['dateObtentionPermis']);

        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }

    public function login($email, $password) {
        $this->db->query("SELECT * FROM utilisateur WHERE email = :email");
        $this->db->bind(':email', $email);

        $row = $this->db->single();

        $hashed_password = $row->mdp;
        if(password_verify($password, $hashed_password)){
            return $row;
        } else {
            return false;
        }
    }
}