<?php

class Vehicule
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getVehiculesByStationId($id){
        $this->db->query("SELECT * FROM vehicule INNER JOIN typevehicule ON typevehicule.codeType = vehicule.codeType WHERE numStation = :id");
        $this->db->bind(':id', $id);

        $results = $this->db->resultSet();

        return $results;
    }
}