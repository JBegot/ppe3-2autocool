<?php

class Station
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getStations(){
        $this->db->query("SELECT * FROM station");

        $results = $this->db->resultSet();

        return $results;
    }

    public function getStationById($id){
        $this->db->query("SELECT * FROM station WHERE numStation = :id");
        $this->db->bind(':id', $id);

        $row = $this->db->single();

        return $row;
    }

    public function getVoiriesById($id){
        $this->db->query("SELECT * FROM voirie WHERE numStation = :id");
        $this->db->bind(':id', $id);

        $results = $this->db->resultSet();

        return $results;
    }

    public function getParkingsById($id){
        $this->db->query("SELECT * FROM parking WHERE numStation = :id");
        $this->db->bind(':id', $id);

        $results = $this->db->resultSet();

        return $results;
    }
}