<?php

class Reserver
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    public function insert($data){
        $this->db->query("INSERT INTO reserver (numVehicule, numUtilisateur, dateReservation, heureReservation, dateDebut, dateFin, heureDebut, heureFin, etatProprete, degats)
                               VALUES (:numVehicule, :numUtilisateur, :dateReservation, :heureReservation, :dateDebut, :dateFin, :heureDebut, :heureFin, :etatProprete, :degats)");

        $this->db->bind(':numVehicule', $data['vehicules']);
        $this->db->bind(':numUtilisateur', $data['numUtilisateur']);
        $this->db->bind(':dateReservation', date('Y-m-d'));
        $this->db->bind(':heureReservation', date('H:i:s'));
        $this->db->bind(':dateDebut', $data['dateDebut']);
        $this->db->bind(':dateFin', $data['dateFin']);
        $this->db->bind(':heureDebut', $data['heureDebut']);
        $this->db->bind(':heureFin', $data['heureFin']);
        $this->db->bind(':etatProprete', NULL);
        $this->db->bind(':degats',NULL);

        if($this->db->execute()){
            return true;
        } else {
            return false;
        }
    }

    public function getReservationsByUser($id){
        $this->db->query("SELECT * FROM reserver INNER JOIN vehicule ON vehicule.numVehicule = reserver.numVehicule WHERE numUtilisateur = :numUtilisateur");
        $this->db->bind(':numUtilisateur', $id);

        $results = $this->db->resultSet();

        return $results;
    }
}